﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpPickaxe : MonoBehaviour
{
    public Manager manager;
    [SerializeField]
    private GameObject Self;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        manager.PickaxeCounter = 5;
        manager.PickaxeActive = true;
        manager.Pickaxe.SetActive(true);
        Destroy(Self);
    }
}
