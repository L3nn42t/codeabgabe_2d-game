﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public int counter;
    public Text Score;

    public bool PickaxeActive;
    public int PickaxeCounter = 5;
    public GameObject Pickaxe;

    public int CounterMax;
    public string scenename;
    
    // Soll beim start einer neuen Scene denn highscore der letzten Szene abrufen
    void Start()
    {
        Score.text = PlayerPrefs.GetInt("Highscore", counter).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        Score.text = counter.ToString();
        if(PickaxeCounter <= 0)
        {
            PickaxeActive = false;
            Pickaxe.SetActive(false);
        }
        if(counter >= CounterMax)
        {
            PlayerPrefs.SetInt("Highscore", counter);
            SceneManager.LoadScene(scenename);
        }
    }
   //Wip Methode, um bei spielstart Highscore auf 0 zu setzen, eher in seperatem skript fürs main menu
}
