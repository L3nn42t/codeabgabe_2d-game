﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBattery : MonoBehaviour
{
    public float Charge;
    public float Drain;
    public GameObject Light;

    public void FixedUpdate()
    {
        Charge -= Drain;
        if(Charge <= 0)
        {
            Light.SetActive(false);
        }
       
    }
}
