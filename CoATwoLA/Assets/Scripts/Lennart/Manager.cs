﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public int counter; // counter, welcher misst, mit wievielen "Diamanten" in diesem fall interagiert wurde
    public Text Score; // zeigt den counter in der UI an 

    public bool PickaxeActive; // Bool der angibt, of Aubgebaut werden kann oder nicht
    public int PickaxeCounter = 5; //Counter, der angeben soll, wieviel die Pickaxe noch aushält
    public GameObject Pickaxe; // Pickaxe des Spielers

    public int CounterMax; // Variable, die wenn der counter sie erreichen muss, damit die nachste Szene geladen wird
    public string scenename;// Name der zu ladenen Szene
    
    // Soll beim start einer neuen Scene denn highscore der letzten Szene abrufen
    void Start()
    {
        Score.text = PlayerPrefs.GetInt("Highscore").ToString(); //Sorgt dafür, dass der aktuelle Highscore beim Szenenwechsel im Highscoremenü angezeigt wird und nicht der default counter
        counter = PlayerPrefs.GetInt("Highscore");//Sorgt dafür, dass der aktuelle Highscore beim Szenenwechsel aktualisiert wird
    }

    // Update is called once per frame
    void Update()
    {
        PlayerPrefs.SetInt("Highscore", counter); // Speichert den derzeitigen Counter ab, beim Szenenwechsel bleibt dieser konstant, wäre vermutlich besser beim Szenenwechsel, aber ist für mich(Lennart) noch zu unzuverlässig
        Score.text = counter.ToString(); // wandelt den derzeitigen score in Schrif um und lässt den Text Score diesen anzeigen
        if(PickaxeCounter <= 0) // Wenn der Pickaxe-Counter 0 erreicht, wird die bool PickaxeActive auf false gestellt, wodurch keine Diamanten mehr abgebaut werden können und die Spitzhacke des Spielers wird nicht gerendert
        {
            PickaxeActive = false;
            Pickaxe.SetActive(false);
        }
        if(counter >= CounterMax)
        {
            
            SceneManager.LoadScene(scenename);
        }
    }
   //Wip Methode, um bei spielstart Highscore auf 0 zu setzen, eher in seperatem skript fürs main menu
   //StartGame
}
