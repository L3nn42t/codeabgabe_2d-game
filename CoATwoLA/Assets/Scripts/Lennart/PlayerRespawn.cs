﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    public GameObject Player;
    public Transform Respawn;
    public float Falldeath;
     

    // Update is called once per frame
    void Update()
    {
        if(Player.transform.position.y <= Falldeath)
        {
            Player.transform.position = Respawn.position;
        }
    }
}
