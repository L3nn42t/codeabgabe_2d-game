﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpPickaxe : MonoBehaviour
{
    public Manager manager;
    [SerializeField]
    private GameObject Self;

    private void OnTriggerEnter2D(Collider2D collision) // bring bei Kollision den Pickaxecounter vom Manager wieder auf 5 und macht die Pickaxe wieder aktiv, anschlißend zerstört es sich selbst, dafür wäre destroy(this) besser gewesen
    {
        manager.PickaxeCounter = 5;
        manager.PickaxeActive = true;
        manager.Pickaxe.SetActive(true);
        Destroy(Self);
    }
}
