﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightBattery : MonoBehaviour
{
    public float Charge; // Wert, welche die ladung einer Batterie simulieren soll
    public float Drain; // Wert, welcher den Verbrauch simmuliert
    public GameObject Light; // Licht, was deaktiviert wird, wenn Charge 0 erreicht
    public Slider slider; // Zeigt in der UI an, wieviel wert noch bei Charge da ist

    public void FixedUpdate()
    {
        Charge -= Drain; // regelmäßige abnahme von Charge un Drain, *Time.DeltaTime wäre besser gewesen, da so sehr niedrige Werte verwendet werden müssen
        if(Charge <= 0)// wenn Charge kleinergleich 0 wird das Licht deaktiviert
        {
            Light.SetActive(false);
        }
        else //ansonsten ist es aktiv
        {
            Light.SetActive(true);
        }
        slider.value = Charge; // slider wird mit dem wert von Charge aktualisiert
    }
}
