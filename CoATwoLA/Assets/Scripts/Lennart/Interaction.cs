﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public Manager manager;
   

    public GameObject player;
    public GameObject Self;
    public GameObject text;
    [SerializeField]
    public static float DistancetoPlayer;
    public float ToPlayer;
    [SerializeField]
    private float IDistance = 1;
    // Update is called once per frame
    void Update()
    {
        Vector2 direction = (player.transform.position - transform.position);

        Debug.DrawRay(transform.position, direction, Color.blue);

        LayerMask mask = LayerMask.GetMask("Player");
        RaycastHit2D PHit = Physics2D.Raycast(transform.position, direction, Mathf.Infinity, mask); // Misst Distanz zwischen Spieler und Objekt, Collider müssen auf ignore Raycast gestellt werden
        ToPlayer = PHit.distance;
        DistancetoPlayer = ToPlayer;

        
            

        if(DistancetoPlayer <= IDistance) // Wenn distanz kleinergleich als IDistance kann durch die Benutzung des Keycode Action der Counter erhoht werden und das Objekt wird zerstört
        {
            
            if (manager.PickaxeActive == true) //Manager braucht Pickaxe, fixed
            {
                
                text.SetActive(true);
                if (Input.GetButtonDown("Action"))
                {
                    text.SetActive(false);
                    manager.counter++;
                    manager.PickaxeCounter--;
                    Destroy(Self);
                }
            }
            
        }

        

            

        

        
    }
}
