﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpLight : MonoBehaviour
{
    public LightBattery battery;
    [SerializeField]
    private GameObject self;
    [SerializeField]
    private float Recharge;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        battery.Charge += Recharge;
        Destroy(self);
    }
}
