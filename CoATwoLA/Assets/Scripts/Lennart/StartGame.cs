﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    [SerializeField]
    private string sceneName;

    public void GameStart()
    {
        PlayerPrefs.SetInt("Highscore", 0);// Soll PlayerPrefs beim Spielstart auf 0 setzen
        SceneManager.LoadScene(sceneName);

    }
}
