﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{

    [HideInInspector] public bool facingRight = true;
    public bool jump = false;
    public float moveSpeed = 300f; // Bewegungsgeschwindkeit des Spielers
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;


    private bool grounded = false;
    public Animator anim;
    private Rigidbody2D rb2d;


    private void Awake()
    {
        //anim = GetComponent<Animator>(); //Placeholder für Animation
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")); // Chect, ob es ein Objekt im Layer Ground zwischen Objekt und Position groundCheck

        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h)); // wenn h größer ist als das float Speed des Animators, wird die Animation Walk abgespielt

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveSpeed); // Wenn a oder d gedrückt werden, wird der Spielerin die jeweilige Richtung beschleunigt, bis die Bewegungsgeschwindigkeit erreicht ist

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump == true)// fügt Kraft nach oben hinzu
        {
            
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
            //anim.SetTrigger("Still");
        }

    }

    void Flip() //invertiert das Spielerobjekt, wenn die Richtung(links-rechts) gewechselt wird
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
