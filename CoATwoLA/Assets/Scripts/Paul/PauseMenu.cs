﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public bool GameISPaused = true;
    public GameObject pauseMenuUI;
    

    private void Update()
    { 

        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
           
            if (GameISPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameISPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0.5f;
        GameISPaused = true;

    }






}
