﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighscoreButton : MonoBehaviour
{
    public GameObject HighscoreUI;
    public GameObject PauseMenuUI;

    public void HighscoreFromMenu()
    {
        PauseMenuUI.SetActive(false);
        HighscoreUI.SetActive(true);
    }
}
