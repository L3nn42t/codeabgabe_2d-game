﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class OnTriggerTextUI : MonoBehaviour {

    //Jeder Trigger braucht einen Box Collider dessen Is Trigger komponente auf true gestellt ist//
    //Der Text welcher dargestellt werden soll ist TextUI, welcher einfach nur ein aktivierter Canvas mit einem deaktiviertem Panel ist//
    //Das Skript aktiviert bei jeglicher Collision mit einem Rigidbody dieses Event//
    [SerializeField]
    Light PointLight;
    public GameObject PowerUP;

    private void OnTriggerEnter2D()
    {
       Destroy(gameObject);
        PointLight.intensity += 10; 
    }
    //Soll die Lichtintensität bei Kollision erhöhen, missnamed(Lennart)
    

}
