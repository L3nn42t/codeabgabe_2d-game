﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsbuttonScript : MonoBehaviour
{
    [SerializeField]
    GameObject PausemenuUI;
    [SerializeField]
    GameObject QuickOptionsUI;
    public PauseMenu pauseMenu;

    public void QuickOptionsmenu()
    {
        QuickOptionsUI.SetActive(true);
        PausemenuUI.SetActive(false);
        //pauseMenu.GameISPaused = false;
    }
}
