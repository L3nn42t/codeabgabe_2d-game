﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeScript : MonoBehaviour
{
    [SerializeField]
    GameObject PauseMenuUI;
    public PauseMenu pauseMenu;
    

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        pauseMenu.GameISPaused = false;
    }

}
