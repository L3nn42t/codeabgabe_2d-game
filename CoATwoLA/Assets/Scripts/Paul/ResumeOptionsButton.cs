﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeOptionsButton : MonoBehaviour
{
    [SerializeField]
    GameObject PauseMenuUI;
    [SerializeField]
    GameObject OptionsUI;
    public PauseMenu pauseMenu;

    public void ResumeButtonOptionsMenu()
    {

        PauseMenuUI.SetActive(true);
        OptionsUI.SetActive(false);
        //pauseMenu.GameISPaused = false;
    }
}
